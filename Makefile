all:
	qlot exec sbcl --load cl-kernel-boss.asd --eval '(ql:quickload :cl-kernel-boss)' --eval "(sb-ext:save-lisp-and-die #p\"cl-kernel-boss\" :toplevel #'cl-kernel-boss:main :executable t)"

clean:
	rm -rf cl-kernel-boss

build-container:
	podman build . -t kernel-boss

run-container:
	podman run --env 'JIRA_KEY*'   --rm -it  localhost/kernel-boss:latest

